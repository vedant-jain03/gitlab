# frozen_string_literal: true

module ComplianceManagement
  module ComplianceFramework
    class ProjectControlComplianceStatus < ApplicationRecord
      belongs_to :compliance_requirements_control
      belongs_to :project
      belongs_to :namespace
      belongs_to :compliance_requirement

      enum status: {
        pass: 0,
        fail: 1,
        pending: 2
      }

      validates :project_id, uniqueness: { scope: :compliance_requirements_control_id }
      validates_presence_of :status, :project, :namespace, :compliance_requirement,
        :compliance_requirements_control

      scope :for_project_and_control, ->(project_id, control_id) {
        where(project_id: project_id, compliance_requirements_control_id: control_id)
      }

      def self.create_or_find_for_project_and_control(project, control)
        record = for_project_and_control(project.id, control.id).first
        return record if record.present?

        create!(
          compliance_requirements_control: control,
          project: project,
          compliance_requirement_id: control.compliance_requirement_id,
          namespace_id: project.namespace_id,
          status: :pending
        )
      rescue ActiveRecord::RecordNotUnique, ActiveRecord::RecordInvalid => e
        if e.is_a?(ActiveRecord::RecordNotUnique) ||
            (e.is_a?(ActiveRecord::RecordInvalid) && e.record&.errors&.of_kind?(:project_id, :taken))
          for_project_and_control(project.id, control.id).first
        else
          raise e
        end
      end
    end
  end
end
